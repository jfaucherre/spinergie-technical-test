import errno
import json
import re
import sys


ARRAY_INDEX_REGEXP = re.compile('\[([0-9]+)\]')


def main():
    if len(sys.argv) != 3:
        print('Usage: python %s <JSON file> <changes file>' % (sys.argv[0]))

    try:
        json_file, changes_file = sys.argv[1:]
        obj = transform_object(json_file, changes_file)
        print(json.dumps(obj))

    except ValueError as err:
        print('Invalid JSON %s' % (err))
    except KeyError as err:
        print('Invalid key %s' % err)
    except IOError as err:
        if err.errno == errno.ENOENT:
            print(err)


def transform_object(json_file, changes_file):
    obj = read_JSON(json_file)
    changes = read_changes(changes_file)

    for keys, value in changes:
        apply_change(keys, value, obj)

    return obj


def apply_change(keys, value, obj):
    if len(keys) == 0:
        return value

    key = keys[0]
    obj[key] = apply_change(keys[1:], value, obj[key])
    return obj


def parse_change(line):
    semi_colon_index = line.index(':')
    keys = line[1:semi_colon_index - 1]
    # add a point before numeric indexes to help parsing them
    keys = ARRAY_INDEX_REGEXP.sub(r'.[\1]', keys)
    # parse indexes
    keys = [k if not k.startswith('[') else int(k[1:-1]) - 1 for k in keys.split('.')]
    # remove eventual first empty key
    if len(keys) > 0 and keys[0] == '':
        keys = keys[1:]
    value = json.loads(line[semi_colon_index + 1:])

    return keys, value


def read_changes(filename):
    with open(filename) as f:
        for line in f:
            yield parse_change(line)


def read_JSON(filename):
    with open(filename) as f:
        return json.loads(f.read())

if __name__ == '__main__':
    main()
