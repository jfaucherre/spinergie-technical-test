import json
from os.path import join
import os
from script import transform_object
import unittest

TEST_ROOT = 'test_files'


def call_on_folder(p):
    json_file, changes_file = join(p, 'obj.json'), join(p, 'changes')
    with open(join(p, 'expected.json')) as f:
        expected = json.loads(f.read())
        return transform_object(json_file, changes_file), expected


class TestSpinergie(unittest.TestCase):
    def test_cases(self):
        for root, dirs, files in os.walk(join('.', TEST_ROOT)):
            for name in dirs:
                result, expected = call_on_folder(join(root, name))
                self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
