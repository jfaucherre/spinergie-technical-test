# Spinergie technical test

This repository holds the result of the technical test for the [Spinergie entreprise](https://www.spinergie.com/)  
It is implemented in python and needs no external dependencies

### Usage

##### Running

You can run the script on any 2 files by running
```shell
python script.py <JSON file> <changes file>
```

##### Testing

You can run all the test files that in `./test_files` by running
```shell
python test.py
```
